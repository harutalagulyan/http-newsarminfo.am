<?php
/* @var $model \common\models\Blog */
?>
<div class="mb-30 sided-250x card-view">
    <div class="s-left">
        <img src="<?=$model->getImage($model::LIST_SIZE)?>" alt="">
    </div><!-- left-area -->
    <div class="s-right ptb-50 p-sm-20 pb-sm-5 plr-30 plr-xs-0">
        <h4>
            <a href="#">
                <?= $model->title ?>
            </a>
        </h4>
        <ul class="mtb-10 list-li-mr-20 color-lite-black">
            <li>
                <i class="mr-5 font-12 ion-clock"></i>
                <?php echo date('d',strtotime($model->created_date));?>
                <?php echo $model::MONTHS[date('m',strtotime($model->created_date))];?>
                <?php echo date('Y',strtotime($model->created_date));?>
            </li>
            <li>
                <i class="mr-5 font-12 ion-eye"></i>
                <?=$model->view?>
            </li>
        </ul>
    </div><!-- right-area -->
</div><!-- sided-250x -->