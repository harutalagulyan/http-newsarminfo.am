<?php

/* @var $this yii\web\View */
$this->title = 'My Yii Application';

?>

<!-- START OF MAIN SLIDER -->
<section class="pt-0 bg-primary">
    <?php echo $this->render('_search')?>


    <div class="plr-50 h-600x h-md-800x h-xs-1000x oflow-hidden">
        <?php echo $this->render('_slider', ['sliders' => $sliders])?>

        <?php echo $this->render('_latest', ['model' => $latest])?>
    </div><!-- wrapper -->
</section>
<!-- END OF MAIN SLIDER -->

<section class="">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-lg-8">
                <?php foreach ($blog_list as $blog) {?>
                    <?php echo $this->render('//product/_blog_item', ['model' => $blog])?>
                <?php } ?>
            </div><!-- col-sm-8 -->

            <div class="col-md-12 col-lg-4">
                <div class="mb-30 p-30 card-view">
                    <h4 class="p-title">
                        <b>STAY CONNECT</b>
                    </h4>
                    <?php echo $this->render('_most_viewed', ['model' => $blog_most_viewed])?>
                </div><!-- card-view -->

            </div><!-- col-sm-4 -->
        </div><!-- row -->
    </div><!-- container -->
</section>

<!-- START OF VIDEO -->
<section class="bg-primary color-white pb-20">
    <div class="container">
        <h4 class="p-title in"><b>FEATIRED VIDEO</b></h4>

        <div class="row">
            <div class="col-sm-6 col-md-3">
                <a class="hover-grey dplay-block" href="#">
                    <div class="pos-relative"><img src="images/video-1-300x300.jpg" alt="">
                        <div class="hover-video"><span class="icon"><i class="ion-play"></i></span></div>
                    </div>

                    <h5 class="mt-15"><b>Dan carter rolls back years while pro dominance leaves England</b></h5></a>
                <ul class="mt-5 mb-30 list-li-mr-20 color-lite-black">
                    <li><i class="mr-5 font-12 ion-clock"></i>Jan 25, 2018</li>
                    <li><i class="mr-5 font-12 ion-eye"></i>105</li>
                </ul>
            </div><!-- col-md-3 -->

            <div class="col-sm-6 col-md-3">
                <a class="hover-grey dplay-block" href="#">
                    <div class="pos-relative"><img src="images/video-2-300x300.jpg" alt="">
                        <div class="hover-video"><span class="icon"><i class="ion-play"></i></span></div>
                    </div>

                    <h5 class="mt-15"><b>Josua VS Parker: Worldcup winners and a mascat</b></h5></a>
                <ul class="mt-5 mb-30 list-li-mr-20 color-lite-black">
                    <li><i class="mr-5 font-12 ion-clock"></i>Jan 25, 2018</li>
                    <li><i class="mr-5 font-12 ion-eye"></i>105</li>
                </ul>
            </div><!-- col-md-3 -->

            <div class="col-sm-6 col-md-3">
                <a class="hover-grey dplay-block" href="#">
                    <div class="pos-relative"><img src="images/video-3-300x300.jpg" alt="">
                        <div class="hover-video"><span class="icon"><i class="ion-play"></i></span></div>
                    </div>

                    <h5 class="mt-15"><b>Aviation summit to be hosted April 16 & 17 at the hilton
                            American-Houston</b></h5></a>
                <ul class="mt-5 mb-30 list-li-mr-20 color-lite-black">
                    <li><i class="mr-5 font-12 ion-clock"></i>Jan 25, 2018</li>
                    <li><i class="mr-5 font-12 ion-eye"></i>105</li>
                </ul>
            </div><!-- col-md-3 -->

            <div class="col-sm-6 col-md-3">
                <a class="hover-grey dplay-block" href="#">
                    <div class="pos-relative"><img src="images/video-4-300x300.jpg" alt="">
                        <div class="hover-video"><span class="icon"><i class="ion-play"></i></span></div>
                    </div>

                    <h5 class="mt-15"><b>Srilanka has a secret lake local aren't tell you absout</b></h5></a>
                <ul class="mt-5 mb-30 list-li-mr-20 color-lite-black">
                    <li><i class="mr-5 font-12 ion-clock"></i>Jan 25, 2018</li>
                    <li><i class="mr-5 font-12 ion-eye"></i>105</li>
                </ul>
            </div><!-- col-md-3 -->
        </div><!-- row -->
    </div><!-- container -->
</section>
<!-- END OF VIDEO -->

<section class="pb-20">
    <div class="container">
        <div class="row">
            <?php echo $this->render('_categories_item', ['model' => $categories])?>
        </div><!-- row -->
    </div><!-- container -->
</section>
