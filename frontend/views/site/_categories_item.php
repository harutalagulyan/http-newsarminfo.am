<?php
/* @var $value \common\models\Categories */
/* @var $blog \common\models\BlogHasCategory */
?>
<?php foreach ($model as $value) {?>
    <div class="col-lg-6">
        <!-- END OF POLITICS -->
        <div class="p-30 card-view">
            <h4 class="p-title">
                <b>
                    <?= $value->title?>
                </b>
            </h4>
            <?php if (isset($value->blogHasCategories[0])){?>
                <div class="row">
                    <div class="col-sm-6">
                        <img src="<?=$value->blogHasCategories[0]->blogForCategoryHomePage->getImage($value->blogHasCategories[0]->blogForCategoryHomePage::CATEGIORY_SIZE)?>" alt="">
                    </div><!-- col-sm-6 -->

                    <div class="col-sm-6">
                        <h4 class="mt-30">
                            <a href="#">
                                <b>
                                    <?=$value->blogHasCategories[0]->blogForCategoryHomePage->title?>
                                </b>
                            </a>
                        </h4>
                        <ul class="mtb-10 list-li-mr-5 color-lite-black">
                            <li><i class="mr-5 font-12 ion-clock"></i>
                                <?php echo date('d',strtotime($value->blogHasCategories[0]->blogForCategoryHomePage->created_date));?>
                                <?php echo $value->blogHasCategories[0]->blogForCategoryHomePage::MONTHS[date('m',strtotime($value->blogHasCategories[0]->blogForCategoryHomePage->created_date))];?>
                                <?php echo date('Y',strtotime($value->blogHasCategories[0]->blogForCategoryHomePage->created_date));?>
                            </li>
                            <li>
                                <i class="mr-5 font-12 ion-eye"></i>
                                <?=$value->blogHasCategories[0]->blogForCategoryHomePage->view?>
                            </li>
                        </ul>
                        <p>
                            <?php echo mb_substr(strip_tags(html_entity_decode($value->blogHasCategories[0]->blogForCategoryHomePage->description, ENT_QUOTES, 'UTF-8')), 0, 250) . '..'?>
                        </p>
                    </div><!-- col-sm-6 -->
                </div><!-- row -->
                <div class="mtb-30 brdr-grey-1"></div>
            <?php } ?>


            <div class="row">
                <?php foreach ($value->blogHasCategories as $key => $blog) {?>
                    <?php if ($key != 0) { ?>
                        <div class="col-sm-6">
                            <div class="brdr-l-grey-2 pl-20 mb-30">
                                <h5>
                                    <a href="#">
                                        <b>
                                            <?= $blog->blog->title?>
                                        </b>
                                    </a>
                                </h5>
                                <ul class="mtb-5 list-li-mr-20 color-lite-black">
                                    <li>
                                        <i class="mr-5 font-12 ion-clock"></i>
                                        <?php echo date('d',strtotime($blog->blog->created_date));?>
                                        <?php echo $blog->blog::MONTHS[date('m',strtotime($blog->blog->created_date))];?>
                                        <?php echo date('Y',strtotime($blog->blog->created_date));?>
                                    </li>
                                    <li>
                                        <i class="mr-5 font-12 ion-eye"></i>
                                        <?= $blog->blog->view?>
                                    </li>
                                </ul>
                            </div><!-- brdr-left-1 -->
                        </div><!-- col-sm-6 -->
                    <?php } ?>
                <?php } ?>
            </div><!-- row -->
        </div><!-- p-30 card-view -->
        <!-- END OF POLITICS -->
    </div>
<?php } ?>