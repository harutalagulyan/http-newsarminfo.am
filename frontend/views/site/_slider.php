<?php
/* @var $this \frontend\controllers\SiteController */
/* @var $sliders[] common\models\Slider */
/* @var $slider common\models\Slider */
?>
<div class="w-60 w-md-100 float-left float-md-none h-100 h-md-40 h-xs-50">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach ($sliders as $key => $slider){?>
                <div class="carousel-item<?= ($key == 0) ? ' active': ''?>">
                    <img class="d-block w-100" src="<?=$slider->getImage()?>" alt="First slide">
                </div>
            <?php } ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>