<?php
/* @var $value \common\models\Blog */
?>
<?php foreach ($model as $value) { ?>
    <div class="sided-80x mb-20">
        <div class="s-left">
            <img src="<?=$value->getImage($value::MOST_SIZE)?>" alt="<?=$value->title?>">
        </div><!-- s-left -->

        <div class="s-right">
            <h5>
                <a href="#">
                    <b>
                        <?=$value->title?>
                    </b>
                </a>
            </h5>
            <ul class="mtb-5 list-li-mr-20 color-lite-black">
                <li>
                    <i class="mr-5 font-12 ion-clock"></i>
                    <?php echo date('d',strtotime($value->created_date));?>
                    <?php echo $value::MONTHS[date('m',strtotime($value->created_date))];?>
                    <?php echo date('Y',strtotime($value->created_date));?>
                </li>
                <li>
                    <i class="mr-5 font-12 ion-eye"></i>
                    <?= $value->view?>
                </li>
            </ul>
        </div><!-- s-left -->
    </div><!-- sided-80x -->
<?php } ?>