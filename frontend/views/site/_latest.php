<?php
/* @var $value \common\models\Blog */
?>
<div class="w-40 w-md-100 float-left float-md-none h-100 h-md-60 h-xs-50">

    <?php foreach ($model as $value) {?>
        <div class="h-50 pb-5 pt-md-10">
            <div class="h-100 pos-relative">
                <!-- Image as bg-3 -->
                <div class="img-bg bg-grad-layer-6" style="background: url(<?=$value->getImage($value::LATEST_SIZE)?>) no-repeat center;background-size: cover;"></div>

                <div class="abs-blr color-white p-20">
                    <h3 class="mb-10 mb-sm-5 t-upper">
                        <a class="hover-grey" href="#">
                            <b>
                                <?= $value->title?>
                            </b>
                        </a>
                    </h3>
                    <ul class="list-li-mr-10 color-ash">
                        <li>
                            <i class="mr-5 font-12 ion-clock"></i>
                            <?php echo date('d',strtotime($value->created_date));?>
                            <?php echo $value::MONTHS[date('m',strtotime($value->created_date))];?>
                            <?php echo date('Y',strtotime($value->created_date));?>
                        </li>
                        <li>
                            <i class="mr-5 font-12 ion-eye"></i>
                            <?= $value->view?>
                        </li>
                    </ul>
                </div><!--abs-blr -->
            </div><!-- h-50 -->
        </div><!-- h-50 -->
    <?php } ?>
</div><!-- w-40 -->