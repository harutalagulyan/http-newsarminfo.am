<header>
    <div class="top-header">
        <div class="container">
            <div class="oflow-hidden font-9 text-sm-center ptb-sm-5">

                <ul class="float-left float-sm-none list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-10 list-a-ptb-xs-5">
                    <li>
                        <?= \frontend\components\Helpers::$week_days_arm[date('l')]?>,
                        <?php echo date('d');?>
                        <?php echo \frontend\components\Helpers::$months_arm[date('m')];?>,
                        <?php echo date('Y');?>
                    </li>
                </ul>
                <ul class="float-right float-sm-none font-13 list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-5 color-ash">
                    <?php if (isset($this->context->settings['socials'])) {?>
                        <?php foreach ($this->context->settings['socials'] as $social) {?>
                            <?php if ($social['status'] == 1){?>
                                <li>
                                    <a class="pl-0 pl-sm-10" href="<?=$social['url']?>" target="_blank">
                                        <i class="ion-social-<?=$social['icon']?>"></i>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>

            </div><!-- top-menu -->
        </div><!-- container -->
    </div><!-- top-header -->

    <div class="middle-header mtb-20 mt-xs-0">
        <div class="container">
            <div class="row">

                <div class="col-sm-4">
                    <a class="logo" href="<?= \yii\helpers\Url::to('/')?>">
                        <img src="images/logo-black.png" alt="Logo">
                    </a>
                </div><!-- col-sm-4 -->

                <div class="col-sm-8">
                    <!-- Bannner bg -->
                    <div class="banner-area dplay-tbl plr-30 plr-md-10 color-white hide">
                        <div class="ptb-sm-15 dplay-tbl-cell">
                            <h5>realestate.com.au</h5>
                            <h6>Discover the latest properties of australia</h6>
                        </div><!-- left-area -->

                        <div class="dplay-tbl-cell text-right min-w-100x">
                            <a class="btn-fill-white btn-b-sm plr-10" href="#">READ MORE</a>
                        </div><!-- right-area -->
                    </div><!-- banner-area -->
                </div><!-- col-sm-4 -->

            </div><!-- row -->
        </div><!-- container -->
    </div><!-- top-header -->

    <div class="bottom-menu">
        <div class="container">

            <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>

            <?= \yii\widgets\Menu::widget(
                [
                    'options'=>['class'=>'main-menu', 'id' => 'main-menu'],
                    'items' => $this->context->menu,
                    'submenuTemplate' => "\n<ul class='drop-down-menu drop-down-inner'>\n{items}\n</ul>\n",
                    'encodeLabels' => false, //allows you to use html in labels
                    'activateParents' => true,
                ]
            ) ?>
            <div class="clearfix"></div>
        </div><!-- container -->
    </div><!-- bottom-menu -->
</header>
