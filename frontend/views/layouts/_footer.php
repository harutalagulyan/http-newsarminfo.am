<footer class="bg-191 pos-relative color-ccc bg-primary pt-50">
    <div class="abs-tblr pt-50 z--1 text-center">
        <div class="h-80 pos-relative"><div class="bg-map abs-tblr opacty-1"></div></div>
    </div>

    <div class="container">

        <div class="mt-20 brdr-ash-1 opacty-4"></div>

        <div class="text-center ptb-30">
            <div class="row">
                <div class="col-sm-3">
                    <a class="mtb-10" href="#"><img src="images/logo-white.png" alt=""></a>
                </div><!-- col-sm-3 -->

                <div class="col-sm-5">
                    <p class="mtb-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur placerat
                        tincidunt mauris semper lorem</p>
                </div><!-- col-sm-3 -->

                <div class="col-sm-4">
                    <ul class="mtb-10 font-12 list-radial-35 list-li-mlr-3">
                        <?php if (isset($this->context->settings['socials'])) {?>
                            <?php foreach ($this->context->settings['socials'] as $social) {?>
                                <?php if ($social['status'] == 1){?>
                                    <li>
                                        <a class="pl-0 pl-sm-10" href="<?=$social['url']?>" target="_blank">
                                            <i class="ion-social-<?=$social['icon']?>"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div><!-- col-sm-3 -->
            </div><!-- row -->
        </div><!-- text-center -->
    </div><!-- container -->

    <div class="bg-dark-primary ptb-15 text-left">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <p class="text-md-center font-9 pt-5 mtb-5"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ion-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div><!-- col-sm-3 -->
            </div><!-- row -->

        </div><!-- container -->
    </div><!-- container -->
</footer>