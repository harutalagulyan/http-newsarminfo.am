<?php
namespace frontend\components;
class Helpers{
    public static $week_days_arm = [
        'Monday'    => 'Երկուշաբթի',
        'Tuesday'   => 'Երեքշաբթի',
        'Wednesday' => 'Չորեքշաբթի',
        'Thursday'  => 'Հինգշաբթի',
        'Friday'    => 'Ուրբաթ',
        'Saturday'  => 'Շաբթի',
        'Sunday'    => 'Կիրակի',
    ];

    public static $months_arm = [
        '01' => 'Հունվար',
        '02' => 'Փետրվար',
        '03' => 'Մարտ',
        '04' => 'Ապրիլ',
        '05' => 'Մայիս',
        '06' => 'Հունիս',
        '07' => 'Հուլիս',
        '08' => 'Օգոստոս',
        '09' => 'Սեպտեմբեր',
        '10' => 'Հոկտեմբեր',
        '11' => 'Նոյեմբեր',
        '12' => 'Դեկտեմբեր',
    ];


    public static function pr($var,$die = false){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if(!$die) {
            die;
        }
    }

    public static function thumb($src, $w = null , $h = null , $q = null) {
        $thumb = \Yii::getAlias('@image').'/timthumb.php';
        $result = $thumb.'?src='.$src;
        if($w){
            $result .= '&w='.$w;
        }
        if($h){
            $result .= '&h='.$h;
        }
        if($q){
            $result .= '&q='.$q;
        }
        return $result;
    }
}
?>