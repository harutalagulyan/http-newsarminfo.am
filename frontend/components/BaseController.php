<?php
/**
 * Created by PhpStorm.
 * User: harut
 * Date: 10/27/18
 * Time: 4:59 PM
 */

namespace frontend\components;


use common\models\Categories;
use common\models\Settings;
use yii\web\Controller;

class BaseController extends Controller
{

    public $menu = array();
    public $settings = array();

    public function beforeAction($action)
    {
        $menu_array = Categories::find()->where('id != 0 AND parent_id = 0 AND status = 1')->with(['categories'])->orderBy('sort')->all();

        foreach ($menu_array as $menu) {
            $items =  array();
            if ($menu->categories) {
                foreach ($menu->categories as $childer) {
                    $items[] = [
                        'label' => $childer->title,
                        'url' => ['/user'],
                    ];
                }
            }
            $this->menu[] =
                [
                    'label' => $menu->title,
                    'template' => $items ? '<a href="{url}" >{label}<i class="ion-chevron-down"></i></a>' : '<a href="{url}" >{label}</a>',
                    'url' => ['/user'],
                    'options'=> ['class' => $items ? 'drop-down' : ''],
                    'items' => $items
                ];
        }

//        Helpers::pr($this->menu);

        $settings = Settings::find()->all();
        foreach ($settings as $setting) {
            $this->settings[$setting->name] = json_decode($setting->data, true);
        }

        return parent::beforeAction($action);
    }
}