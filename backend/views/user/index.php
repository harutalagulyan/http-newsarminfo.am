<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'username',
                'email',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'label' => 'Status',
                    'value' => function ($data) {
                        return Yii::$app->params["label_active"][$data->status];
                    },
                    'filter' => Yii::$app->params['status']
                ],

                //'created_at',
                //'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
//                    'buttons' => [
//                        'delete' => function ($url, $model) {
//                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
//                                [
//                                    'data' => [
//                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//                                        'method' => 'post',
//                                        'pjax' => 0,
//                                        'title' => Yii::t('yii', 'Confirm?'),
//                                        'ok' => Yii::t('yii', 'Confirm'),
//                                        'cancel' => Yii::t('yii', 'Cancel'),
//                                    ],
//                                ]);
//                        }
//                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
