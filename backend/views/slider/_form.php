<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord) {
$this->title = 'Crete Slider';
}else{
$this->title = 'Update Slider' . ' # '. $model->id ;
}

$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="slider-form box box-primary">
    <div class="box-header">
        <?= Html::a('List Slider', ['index', 'id' => $model->id], ['class' =>
        'btn btn-primary btn-flat']) ?>
        <?php  if(!$model->isNewRecord) { ?>
            <?= Html::a('Create New Slider', ['Create'], ['class' =>
            'btn btn-primary btn-flat']) ?>
            <?= Html::a('View Slider'. ' # '. $model->id, ['view', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
            <?= Html::a('Delete Slider'. ' # '. $model->id, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
            ],
            ]) ?>
        <?php  } ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="clearfix">
            <div class="col-sm-8">
                <?php echo $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Вибрат Фото',
                        'initialPreview'=>[
                            (!$model->isNewRecord && $model->image)? $model->getImage() : '',
                        ],
                        'initialPreviewAsData'=>true,
                    ],
                ]);?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'order')->textInput() ?>

                <?= $form->field($model, 'status')->checkbox(['class' => 'minimal']) ?>

            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat
        col-sm-12']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
