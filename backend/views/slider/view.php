<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = 'View Slider # ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view box box-primary">
    <div class="box-header">
        <?= Html::a('List Slider', ['index', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Create New Slider', ['Create'], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Update Slider' . ' # ' . $model->id, ['update', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete Slider' . ' # ' . $model->id, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'order',
                [
                    'label' => 'Status',
                    'attribute' => 'status',
                    'value' => Yii::$app->params['label_active'][$model->status],
                    'format' => 'raw'
                ],
                [
                    'attribute'=>'image',
                    'value'  => $model->getImage(),
                    'format' => ['image'],
                ],
            ],
        ]) ?>
    </div>
</div>
