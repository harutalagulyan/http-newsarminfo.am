<?php
/* @var $this \backend\controllers\SettingsController */
/* @var $form yii\widgets\ActiveForm */
/* @var $data \common\models\Settings */
$socials = json_decode($data->data, true);
$socials = $socials ? $socials : array();

?>

<div class="user-form box box-primary">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'social-form',
        'enableClientValidation'=>false
    ]); ?>
    <div class="box-body table-responsive">
        <?php foreach($socials as $s){
            $model = new \backend\models\SocialsForm();
            $model->attributes = $s;
            ?>
            <div class="social-module">
                <div class="clearfix">
                    <div class="col-sm-5">
                        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-1">
                        <?= $form->field($model, 'status')->checkbox(['class' => 'minimal']) ?>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-danger remove-social margin-top24"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="clearfix">
            <div class="col-md-4 col-md-offset-4 margin-top">
                <button class="btn btn-warning btn-block add-social">Add Social</button>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success btn-flat col-sm-12', 'id' => 'submit-button-social']) ?>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>

<?php $model = new \backend\models\SocialsForm(); ?>
<div class="hidden">
    <div id="social_template">
        <div class="social-module">
            <div class="clearfix">
                <div class="col-sm-5">
                    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, 'status')->checkbox(['class' => 'minimal']) ?>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-danger remove-social margin-top24"><i class="fa fa-remove"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Social Networks Updated.
</div>
<script>
    $(document).ready(function () {
        $('.alert-success').hide();
        $(document).on('click', '.add-social', function (e) {
            e.preventDefault();
            $($('#social_template').html()).insertBefore($(this).closest('div.clearfix'));
        });
        $(document).on('click', '.remove-social', function (e) {
            e.preventDefault();
            $(this).closest('div.social-module').remove();
        });
        $(document).on('click', '#submit-button-social', function (e) {
            e.preventDefault();
            $.each($('.social-module'), function (k, v) {
                $.each($(this).find('input, select'), function () {
                    $(this).attr('name', $(this).attr('name').replace('[', '[' + k + ']['));
                });
            });

            $('#social-form div.error').remove();
            $.ajax({
                url: '<?=\yii\helpers\Url::to(['/settings/savesocial'])?>',
                type: 'post',
                data: $('#social-form').serializeArray(),
                dataType: 'json',
                success: function (data) {
                    $.each($('.social-module'), function (k, v) {
                        $.each($(this).find('input, select'), function () {
                            $(this).attr('name', $(this).attr('name').replace('[' + k + '][', '['));
                        });
                    });
                    if (data.length == 0) {
                        $('.social-module .required.has-error').removeClass('required has-error');
                        $('.social-module .help-block').html('');
                        $('.alert-success').fadeIn('slow').delay(800).fadeOut('slow');
                    } else {
                        $('.social-module .required.has-error').removeClass('required has-error');
                        $('.social-module .help-block').html('');
                        $.each(data, function (ke, va) {
                            $.each(va, function (k, v) {
                                $('.social-module:eq(' + (ke) + ') .field-socialsform-' + k).addClass('required has-error');
                                $('.social-module:eq(' + (ke) + ') .field-socialsform-' + k+ ' .help-block').html(v[0]);
                            });
                        });
                    }
                }
            });
        });
    });
</script>