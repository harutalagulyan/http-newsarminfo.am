<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $this \backend\controllers\SettingsController */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Socials</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Headers</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Tab 3</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <?php echo $this->render('_socials', ['data' => $model['socials']]);?>
        </div>
        <div class="tab-pane" id="tab_2">
            <?php echo $this->render('_headers', ['data' => $model['headers']]);?>
        </div>
    </div>
    <!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->

