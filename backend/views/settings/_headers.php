<?php
/* @var $this \backend\controllers\SettingsController */
/* @var $form yii\widgets\ActiveForm */
/* @var $data \common\models\Settings */
$headers = json_decode($data->data, true);
$headers = $headers ? $headers : array();
?>

<div class="user-form box box-primary">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'header-form',
        'enableClientValidation'=>false
    ]); ?>
    <div class="box-body table-responsive">
        <?php foreach($headers as $s){
            $model = new \backend\models\HeadersForm();
            $model->attributes = $s;
            ?>
            <div class="header-module">
                <div class="clearfix">
                    <div class="col-sm-2">
                        <?= $form->field($model, 'position')->dropDownList([0 => 'Header', 1 => 'Footer']) ?>
                    </div>
                    <div class="col-sm-9">
                        <?= $form->field($model, 'code')->textarea(['rows' => '6']) ?>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-danger remove-header margin-top24"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="clearfix">
            <div class="col-md-4 col-md-offset-4 margin-top">
                <button class="btn btn-warning btn-block add-header">Add Header</button>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success btn-flat col-sm-12', 'id' => 'submit-button-header']) ?>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>

<?php $model = new \backend\models\HeadersForm(); ?>
<div class="hidden">
    <div id="header_template">
        <div class="header-module">
            <div class="clearfix">
                <div class="col-sm-2">
                    <?= $form->field($model, 'position')->dropDownList([0 => 'Header', 1 => 'Footer']) ?>
                </div>
                <div class="col-sm-9">
                    <?= $form->field($model, 'code')->textarea(['rows' => '6']) ?>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-danger remove-header margin-top24"><i class="fa fa-remove"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Headers Networks Updated.
</div>
<script>
    $(document).ready(function () {
        $('.alert-success').hide();
        $(document).on('click', '.add-header', function (e) {
            e.preventDefault();
            $($('#header_template').html()).insertBefore($(this).closest('div.clearfix'));
        });
        $(document).on('click', '.remove-header', function (e) {
            e.preventDefault();
            $(this).closest('div.header-module').remove();
        });
        $(document).on('click', '#submit-button-header', function (e) {
            e.preventDefault();
            $.each($('.header-module'), function (k, v) {
                $.each($(this).find('input,textarea,select'), function () {
                    $(this).attr('name', $(this).attr('name').replace('[', '[' + k + ']['));
                });
            });

            $('#header-form div.error').remove();
            $.ajax({
                url: '<?=\yii\helpers\Url::to(['/settings/save-headers'])?>',
                type: 'post',
                data: $('#header-form').serializeArray(),
                dataType: 'json',
                success: function (data) {
                    $.each($('.header-module'), function (k, v) {
                        $.each($(this).find('input,textarea,select'), function () {
                            $(this).attr('name', $(this).attr('name').replace('[' + k + '][', '['));
                        });
                    });
                    if (data.length == 0) {
                        $('.header-module .required.has-error').removeClass('required has-error');
                        $('.header-module .help-block').html('');
                        $('.alert-success').fadeIn('slow').delay(800).fadeOut('slow');
                    } else {
                        $('.header-module .required.has-error').removeClass('required has-error');
                        $('.header-module .help-block').html('');
                        $.each(data, function (ke, va) {
                            $.each(va, function (k, v) {
                                $('.header-module:eq(' + (ke) + ') .field-headersform-' + k).addClass('required has-error');
                                $('.header-module:eq(' + (ke) + ') .field-headersform-' + k+ ' .help-block').html(v[0]);
                            });
                        });
                    }
                }
            });
        });
    });
</script>