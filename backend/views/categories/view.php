<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'label' => 'Parent',
                    'attribute' => 'parent_id',
                    'value' => $model->parent->title,
                ],
                'title',
                'meta_description:ntext',
                'sort',
                'created_date',
                'modify_date',
                [
                    'attribute' => 'show_in_home_page',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::checkbox('show_in_home_page[]', $model->show_in_home_page, ['value' => $model->show_in_home_page, 'class' => 'minimal']);
                    },

                ],
                [
                    'label' => 'Status',
                    'attribute' => 'status',
                    'value' => Yii::$app->params['label_active'][$model->status],
                    'format' => 'raw'
                ],
                [
                    'attribute'=>'image',
                    'value'  => $model->getImage(),
                    'format' => ['image'],
                ],
            ],
        ]) ?>
    </div>
</div>