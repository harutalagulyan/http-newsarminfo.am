<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord) {
    $this->title = 'Crete Category';
}else{
    $this->title = 'Update Category' . ' # '. $model->id ;
}

$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-form box box-primary">
    <div class="box-header">
        <?= Html::a('List', ['index', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?php  if(!$model->isNewRecord) { ?>
            <?= Html::a('Create', ['create'], ['class' =>
                'btn btn-primary btn-flat']) ?>
            <?= Html::a('View', ['view', 'id' => $model->id], ['class' =>
                'btn btn-primary btn-flat']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php  } ?>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body table-responsive">

        <div class="clearfix">
            <div class="col-sm-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Вибрат Фото',
                        'initialPreview'=>[
                            (!$model->isNewRecord && $model->image)? $model->getImage() : '',
                        ],
                        'initialPreviewAsData'=>true,
                    ],
                ]);?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'parent_id')->dropDownList($model->getParents()) ?>
                <?= $form->field($model, 'sort')->textInput() ?>
                <?= $form->field($model, 'show_in_home_page')->checkbox(['class' => 'minimal']) ?>
                <?= $form->field($model, 'status')->checkbox(['class' => 'minimal']) ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat col-sm-12']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
