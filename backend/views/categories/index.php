<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                 [
                     'attribute' => 'parent_id',
                     'filter' => $parents,
                     'label'=>'Parent category', // col title
                     'value'=>function ($data){
                         return $data->parent->title;
                     }
                ],
                'title',
                'sort',
                'created_date',
                'modify_date',
                [
                    'attribute' => 'show_in_home_page',
                    'format' => 'raw',
                    'label' => 'Show in home page',
                    'value' => function ($data) {

                        return Yii::$app->params["label_yes_no"][$data->show_in_home_page];
                    },
                    'filter' => Yii::$app->params['yes_no']
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'label' => 'Status',
                    'value' => function ($data) {

                        return Yii::$app->params["label_active"][$data->status];
                    },
                    'filter' => Yii::$app->params['status']
                ],
                //'meta_description:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
