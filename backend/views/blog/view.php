<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = 'View Blog # ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view box box-primary">
    <div class="box-header">
        <?= Html::a('List Blog', ['index', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Create New Blog', ['Create'], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Update Blog' . ' # ' . $model->id, ['update', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete Blog' . ' # ' . $model->id, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'categories',
                    'format' => 'raw',
                    'label' => 'Categories',
                    'value' => function ($model) {
                        return $model->getCategoriesListStr();
                    },

                ],
                'title',
                [
                    'attribute' => 'description',
                    'format' => 'raw',
                    'label' => 'Description',

                ],
                'order',
                'created_date',
                'modify_date',
                'available_date',
                'view',
                [
                    'attribute' => 'latest',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::checkbox('latest[]', $model->latest, ['value' => $model->latest, 'class' => 'minimal']);
                    },

                ],
                [
                    'label' => 'Status',
                    'attribute' => 'status',
                    'value' => Yii::$app->params['label_active'][$model->status],
                    'format' => 'raw'
                ],
                [
                    'attribute'=>'image',
                    'value'  => $model->getOriginalImage(),
                    'format' => ['image'],
                ],
            ],
        ]) ?>
    </div>
</div>
