<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogSearch */
/* @var $data common\models\Blog*/
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'order',
                'created_date',
                'modify_date',
                'available_date',
                [
                    'attribute' => 'latest',
                    'format' => 'raw',
                    'label' => 'Latest',
                    'value' => function ($data) {
                        return Yii::$app->params["label_yes_no"][$data->latest];
                    },
                    'filter' => Yii::$app->params['yes_no']
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'label' => 'Status',
                    'value' => function ($data) {

                        return Yii::$app->params["label_active"][$data->status];
                    },
                    'filter' => Yii::$app->params['status']
                ],
                [
                    'attribute' => 'categories',
                    'label' => 'Categories',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->getCategoriesListStr();
                    },
                    'filter' => [
                            4 => 'title 2',
                            5 => 'title 3',
                            6 => 'title 4',
                    ]
                ],
                'title',
                [
                    'format' => 'image',
                    'value'=>function($data) { return $data->getImage(null, 'auto', 100); },

                ],

                //'description:ntext',
                //'image',
                //'view',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
