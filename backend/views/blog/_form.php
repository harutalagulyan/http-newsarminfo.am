<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $this \backend\controllers\BlogController */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
if ($model->isNewRecord) {
    $this->title = 'Crete Blog';
    $model->available_date = date('Y-m-d h:i:s');
} else {
    $this->title = 'Update Blog' . ' # ' . $model->id;
}

$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$categories_array = \common\models\Categories::find()->where('id != 0')->indexBy('id')->asArray()->all();
$categories = \yii\helpers\ArrayHelper::map($categories_array, 'id', 'title');

$selected = array();
if (!$model->isNewRecord){
    $selected = \yii\helpers\ArrayHelper::map(\common\models\BlogHasCategory::find()->where('blog_id = '.$model->id)->all(), 'id', 'category_id');
}

?>
<div class="blog-form box box-primary">
    <div class="box-header">
        <?= Html::a('List Blog', ['index', 'id' => $model->id], ['class' =>
            'btn btn-primary btn-flat']) ?>
        <?php if (!$model->isNewRecord) { ?>
            <?= Html::a('Create New Blog', ['Create'], ['class' =>
                'btn btn-primary btn-flat']) ?>
            <?= Html::a('View Blog' . ' # ' . $model->id, ['view', 'id' => $model->id], ['class' =>
                'btn btn-primary btn-flat']) ?>
            <?= Html::a('Delete Blog' . ' # ' . $model->id, ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <div class="clearfix">
            <div class="col-sm-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6, 'class' => 'editor']) ?>

            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Вибрат Фото',
                        'initialPreview' => [
                            (!$model->isNewRecord && $model->image) ? $model->getOriginalImage() : '',
                        ],
                        'initialPreviewAsData' => true,
                    ],
                ]); ?>
            </div>
            <div class="col-sm-3">
                <?php
                echo \kartik\select2\Select2::widget([
                    'name' => 'Categories',
                    'data' => $categories,
                    'value' => $selected,
                    'options' => [
                        'placeholder' => 'Select provinces ...',
                        'multiple' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'available_date')->textInput(['class' => "datetimepicker form-control"]) ?>

                <?= $form->field($model, 'order')->textInput() ?>

                <?= $form->field($model, 'latest')->checkbox(['class' => 'minimal']) ?>

                <?= $form->field($model, 'status')->checkbox(['class' => 'minimal']) ?>

            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat
        col-sm-12']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>