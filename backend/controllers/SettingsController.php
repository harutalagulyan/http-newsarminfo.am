<?php

namespace backend\controllers;

use backend\models\HeadersForm;
use backend\models\SocialsForm;
use frontend\components\Helpers;
use Yii;
use common\models\Settings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Settings::find()->indexBy('name')->all();

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionSavesocial() {
        if (isset($_POST['SocialsForm'])) {
Helpers::pr($_POST);
            $check = true;
            $errors = array();
            foreach ($_POST['SocialsForm'] as $key => $val) {
                $model = new SocialsForm();
                $model->attributes = $val;
                if (!$model->validate()) {
                    $check = false;
                    $errors[$key] = $model->getErrors();
                }
            }
            if ($check) {
                Settings::updateAll(['data' => json_encode($_POST['SocialsForm'])], 'name = "socials"');
                echo json_encode(array());
            }
            else {
                echo json_encode($errors);
            }
        }
    }
    public function actionSaveHeaders() {
        if (isset($_POST['HeadersForm'])) {
            $check = true;
            $errors = array();
            foreach ($_POST['HeadersForm'] as $key => $val) {
                $model = new HeadersForm;
                $model->attributes = $val;
                if (!$model->validate()) {
                    $check = false;
                    $errors[$key] = $model->getErrors();
                }
            }
            if ($check) {
                Settings::updateAll(['data' => json_encode($_POST['HeadersForm'])], 'name = "headers"');
                echo json_encode(array());
            }
            else {
                echo json_encode($errors);
            }
        }
    }
}
