<?php

namespace backend\controllers;

use common\models\BlogHasCategory;
use frontend\components\Helpers;
use Yii;
use common\models\Blog;
use common\models\BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
////        return [
////            'verbs' => [
////                'class' => VerbFilter::className(),
////                'actions' => [
////                    'delete' => ['POST'],
////                ],
////            ],
////        ];
//    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog();
        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');

            if (!is_null($image)) {
                $model->image = $image->name;
                $ext = $image->extension;
                // generate a unique file name to prevent duplicate filenames
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                $path = $model->getImageDir(). $model->image;
                $image->saveAs($path);
            }
            if ($model->save()) {
                if (Yii::$app->request->post('Categories')){
                    foreach (Yii::$app->request->post('Categories') as $category){
                        $blog_has_category = new BlogHasCategory;
                        $blog_has_category->blog_id = $model->id;
                        $blog_has_category->category_id = $category;
                        $blog_has_category->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $model->image = $image->name;
                $ext = $image->extension;
                // generate a unique file name to prevent duplicate filenames
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                $path = $model->getImageDir(). $model->image;
                if ($image->saveAs($path)){
                    @unlink($model->getImageDir() . $model->oldAttributes['image']);
                }
            }else{
                $model->image = $model->oldAttributes['image'];
            }

            if ($model->save()) {
                BlogHasCategory::deleteAll('blog_id = '.$model->id );
                if (Yii::$app->request->post('Categories')){
                    foreach (Yii::$app->request->post('Categories') as $category){
                        $blog_has_category = new BlogHasCategory;
                        $blog_has_category->blog_id = $model->id;
                        $blog_has_category->category_id = $category;
                        $blog_has_category->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionUpload(){
        if($_FILES['upload']) {
            $callback = 1;
            if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
                $message = "Вы не выбрали файл";
            }

            else if (!is_uploaded_file($_FILES['upload']["tmp_name"])){

                $message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз.";
            }

            else {

                $name = sprintf('%s-%s.%s', rand(1000, 9999), date('dmY'), $this->getex($_FILES['upload']['name']));

                $full_path = Yii::getAlias('@image').'/blogs/'.$name;


                move_uploaded_file($_FILES['upload']['tmp_name'], Yii::getAlias('@image_root') .'/blogs/'.$name);

                $message = "Файл ".$_FILES['upload']['name']." загружен";
            }

//            $callback = $_REQUEST['CKEditorFuncNum'];
            echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "'.$full_path.'", "'.$message.'" );</script>';

        }
//        Helpers::pr($_FILES);
//        $this->layout = false;
//        $this->render('upload');
    }
    function getex($filename) {
        $end = explode(".", $filename);
        return end($end);
    }

    public function actionBrowse(){
//        $this->layout = false;
//
//        $this->render('browse');
    }
}
