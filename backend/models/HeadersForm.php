<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class HeadersForm extends Model
{
    public $position;
    public $code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
}
