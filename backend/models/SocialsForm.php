<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SocialsForm extends Model
{
    public $icon;
    public $url;
    public $status;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['icon', 'url'], 'required'],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
}
