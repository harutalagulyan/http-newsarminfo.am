/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.extraPlugins = 'uploadimage';
    config.imageUploadUrl = '/uploader/upload';
    // config.image = '/uploader/upload.php?type=Images';
    // config.uploadUrl = '/uploader/upload.php?type=Images';
};
