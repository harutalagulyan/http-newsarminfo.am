<?php
namespace dmstr\web;

use frontend\components\Helpers;
use yii\base\Exception;
use yii\web\AssetBundle as BaseAdminLteAsset;
use yii\web\View;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class AdminLteAsset extends BaseAdminLteAsset
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';
    public $css = [
        'css/AdminLTE.min.css',
        'css/custom.css',
        'css/bootstrap-datetimepicker.min.css',
    ];
    public $js = [
        'js/adminlte.min.js',
//        'js/jquery-ui.min.js',
        'js/bootstrap-datetimepicker.js',
        'js/custom.js',
        'ckeditor/ckeditor.js'
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $jsOptions = array(

        'position' => View::POS_HEAD // too high

        //'position' => View::POS_READY // in the html disappear the jquery.jrac.js declaration

        //'position' => View::POS_LOAD // disappear the jquery.jrac.js

//        'position' => View::POS_END // appear in the bottom of my page, but jquery is more down again
    );

    /**
     * @var string|bool Choose skin color, eg. `'skin-blue'` or set `false` to disable skin loading
     * @see https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html#layout
     */
    public $skin = '_all-skins';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (YII_DEBUG){
            foreach ($this->js as $key=>$js){
                $this->js[$key] = $js.'?v='.time();
            }
        }

        // Append skin color file if specified
        if ($this->skin) {
            if (('_all-skins' !== $this->skin) && (strpos($this->skin, 'skin-') !== 0)) {
                throw new Exception('Invalid skin specified');
            }

            $this->css[] = sprintf('css/skins/%s.min.css', $this->skin);
        }

        parent::init();
    }
}
