<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}
$urlParams = $generator->generateUrlParams();
echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord) {
$this->title = <?= $generator->generateString('Crete ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
}else{
$this->title = <?= $generator->generateString('Update ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?> . ' # '. $model->id ;
}

$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form box box-primary">
    <div class="box-header">
        <?= "<?= " ?>Html::a(<?= $generator->generateString('List '.Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['index', <?= $urlParams ?>], ['class' =>
        'btn btn-primary btn-flat']) ?>
        <?= "<?php " ?> if(!$model->isNewRecord) { <?= "?>" ?>

            <?= "<?= " ?>Html::a(<?= $generator->generateString('Create New '.Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['Create'], ['class' =>
            'btn btn-primary btn-flat']) ?>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('View '.Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>. ' # '. $model->id, ['view', <?= $urlParams ?>], ['class' =>
            'btn btn-primary btn-flat']) ?>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('Delete '.Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>. ' # '. $model->id, ['delete', <?= $urlParams ?>], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
            'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
            'method' => 'post',
            ],
            ]) ?>
        <?= "<?php " ?> } <?= "?>" ?>

    </div>
    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?php foreach ($generator->getColumnNames() as $attribute) {
            if (in_array($attribute, $safeAttributes)) {
                echo "        <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
            }
        } ?>
    </div>
    <div class="box-footer">
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, ['class' => 'btn btn-success btn-flat
        col-sm-12']) ?>
    </div>
    <?= "<?php " ?>ActiveForm::end(); ?>
</div>
