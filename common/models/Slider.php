<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property int $order
 * @property int $status
 * @property string $title
 * @property string $image
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'status' => 'Status',
            'title' => 'Title',
            'image' => 'Image',
        ];
    }

    public function getImageDir(){
        return Yii::getAlias('@image_root') .'/sliders/';
    }

    public function getImage($position = 'main'){
        return Yii::getAlias('@image').'/sliders/'.$this->image;
    }
}
