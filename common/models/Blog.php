<?php

namespace common\models;

use frontend\components\Helpers;
use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property int $order
 * @property int $status
 * @property int $latest
 * @property string $created_date
 * @property string $modify_date
 * @property string $available_date
 * @property string $title
 * @property string $description
 * @property string $image
 * @property int $view
 *
 * @property BlogHasCategory[] $blogHasCategories
 */
class Blog extends \yii\db\ActiveRecord
{
    const MAIN_SIZE = [
        'w' => '',
        'h' => ''
    ];

    const LATEST_SIZE = [
        'w' => '600',
        'h' => '300',
        'q' => null
    ];

    const LIST_SIZE = [
        'w' => '250',
        'h' => '250',
        'q' => null
    ];

    const MOST_SIZE = [
        'w' => '100',
        'h' => '100',
        'q' => null
    ];

    const CATEGIORY_SIZE = [
        'w' => '400',
        'h' => '300',
        'q' => null
    ];

    const MONTHS = [
        '01' => 'Հունվար',
        '02' => 'Փետրվար',
        '03' => 'Մարտ',
        '04' => 'Ապրիլ',
        '05' => 'Մայիս',
        '06' => 'Հունիս',
        '07' => 'Հուլիս',
        '08' => 'Օգոստոս',
        '09' => 'Սեպտեմբեր',
        '10' => 'Հոկտեմբեր',
        '11' => 'Նոյեմբեր',
        '12' => 'Դեկտեմբեր',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'status', 'latest', 'view'], 'integer'],
            [['created_date', 'modify_date', 'available_date'], 'safe'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'status' => 'Status',
            'latest' => 'Latest',
            'created_date' => 'Created Date',
            'modify_date' => 'Modify Date',
            'available_date' => 'Available Date',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'view' => 'View',
        ];
    }

    public function beforeSave($insert)
    {
        $this->modify_date = date('Y-m-d h:i:s');
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogHasCategories()
    {
        return $this->hasMany(BlogHasCategory::className(), ['blog_id' => 'id']);
    }

    public function getImageDir(){
        return Yii::getAlias('@image_root') .'/blogs/';
    }

    public function getImage($position, $w = null, $h = null, $q = null){
        if ($position){
            $img = \frontend\components\Helpers::thumb(Yii::getAlias('@image').'/blogs/'.$this->image,$position['w'], $position['h'], $position['q']);
        }else{
            $img = \frontend\components\Helpers::thumb(Yii::getAlias('@image').'/blogs/'.$this->image, $w, $h, $q);
        }
        return $img;
    }

    public function getOriginalImage(){
        return Yii::getAlias('@image').'/blogs/'.$this->image;
    }

    public function getCategoriesListStr(){
        $categories_list= \yii\helpers\ArrayHelper::map($this->blogHasCategories, 'category_id', 'category.title');
        $categories = '';
        foreach ($categories_list as $value){
            $categories .= '<span class="label label-primary label-lg">'. $value .'</span>';
        }
        return $categories;

    }
    public function getCategoriesList(){

        return \yii\helpers\ArrayHelper::map($this->blogHasCategories, 'category_id', 'category.title');
    }
}
