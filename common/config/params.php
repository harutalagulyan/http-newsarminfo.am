<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'yes_no'            => array('0' => 'NO', '1' => 'YES'),
    'status'            => array('0' => 'INACTIVE', '1' => 'ACTIVE'),
    'label_yes_no'      => array('0' => '<span class="label label-inactive label-lg">NO</span>', '1' => '<span class="label label-success label-lg">YES</span>'),
    'label_active'      => array('0' => '<span class="label label-inactive label-lg">INACTIVE</span>', '1' => '<span class="label label-success label-lg">ACTIVE</span>'),
];
